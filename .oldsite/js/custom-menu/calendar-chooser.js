$(document).ready(function() {
	var calendarFolder = "/Schedules/";
	$(".selected").click(function() {
		var chooser = $(this).siblings('ul');
		toggleMenu(chooser);
	});
	var schoolYearSchedules = $(".schedule-chooser").find('li');
	schoolYearSchedules.click(function() {
		var selected = $(this).parent().siblings(".selected");
		selected.attr("value", $(this).attr("value"));
		selected.html($(this).html());
		selected.removeClass("invalid");
		closeMenu($(this).parent('.schedule-chooser'));
	});

	$(".selected").addClass("invalid");
	$("#school-year-button").click(function(event) {
		if ($("#selected-school-year").hasClass("invalid") == false) {
			fileName = $("#selected-school-year").html();
			window.open(calendarFolder + 'School-Year/' + fileName, '_blank');
		}
		event.preventDefault();
	});
	$("#summer-button").click(function(event) {
		if ($("#selected-summer").hasClass("invalid") == false) {
			fileName = $("#selected-summer").html();
			window.open(calendarFolder + 'Summer/' + fileName, "_blank");
		}
		event.preventDefault();
	});
});

function closeMenu(menu) {
	// menu.stop().slideUp(300);
	menu.css("display", "none");
	menu.removeClass("active");
}
function openMenu(menu) {
	// menu.stop().slideDown(300);
	menu.css("display", "block");
	menu.addClass("active");
}
function toggleMenu(menu) {
	if (menu.hasClass("active")) {
		closeMenu(menu);
	} else {
		openMenu(menu);
	}
}