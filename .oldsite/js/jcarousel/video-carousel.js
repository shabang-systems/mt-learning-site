$(document).ready(function() {
	var videoWidth = getWidth($(".centered-video-container").children(".video-frame").children("iframe").attr("width")) + getWidth($(".centered-video-container").children('.video-frame').css("padding-right"));
	var divWidth = $(".centered-video-container").children().length * videoWidth;
    var width = divWidth.toString() + "px";

	$("#scroll-left").click(function() {
		if ($(".centered-video-container").is(":animated")) {
			return false;
		}
		var targetPosition = parseInt($(".centered-video-container").css("left"), 10) + parseInt($(".centered-video-container").children('.video-frame').children("iframe").attr("width"), 10) + parseInt($(".centered-video-container").children('.video-frame').css("padding-right"));
		if (targetPosition <= 0) {
			$(".centered-video-container").animate({"left": (targetPosition.toString() + "px")});
		}
	});
	$("#scroll-right").click(function() {
		if ($(".centered-video-container").is(":animated")) {
			return false;
		}
		var targetPosition = parseInt($(".centered-video-container").css("left"), 10) - parseInt($(".centered-video-container").children('.video-frame').children("iframe").attr("width"), 10) - parseInt($(".centered-video-container").children('.video-frame').css("padding-right"));
		if (Math.abs(targetPosition) < divWidth) {
			$(".centered-video-container").animate({"left": targetPosition.toString() + "px"});
		}
	});
});

function getWidth(widthProperty) {
    return parseInt(widthProperty.substring(0, $(".centered-video-container").css("width").length - 2), 10);
}