/**
 * Created by Administrator on 16/01/2015.
 */
jQuery.validator.addMethod('required_group_name', function(value, element, params) {
    var container =$("ul.panel");
    return container.find('.required_group_name:filled').length;
}, "error message");
jQuery.validator.addClassRules('required_group_name', {
    'required_group_name' : true
});
/*jQuery.validator.messages.required_group_name ="Please at least fill out the Father name or Mother";*/
jQuery.validator.messages.required_group_name ="";


jQuery.validator.addMethod('required_group_phone', function(value, element, params) {
    var container =$("ul.panel");
    return container.find('.required_group_phone:filled').length;
}, "error message");
jQuery.validator.addClassRules('required_group_phone', {
    'required_group_phone' : true
});
/*jQuery.validator.messages.required_group_phone ="Please at least fill out the Father name or Mother";*/
jQuery.validator.messages.required_group_phone ="";



jQuery.validator.addMethod('required_group_home', function(value, element, params) {
    var container =$("ul.panel");
    return container.find('.required_group_home:filled').length;
}, "error message");
jQuery.validator.addClassRules('required_group_home', {
    'required_group_home' : true
});
/*jQuery.validator.messages.required_group_home ="Please at least fill out the Father name or Mother";*/
jQuery.validator.messages.required_group_home ="";


jQuery.validator.addMethod('required_group_mail', function(value, element, params) {
    var container =$("ul.panel");
    return container.find('.required_group_mail:filled').length;
}, "error message");
jQuery.validator.addClassRules('required_group_mail', {
    'required_group_mail' : true
});
/*jQuery.validator.messages.required_group_mail ="Please at least fill out the Father name or Mother";*/
jQuery.validator.messages.required_group_mail ="";
