
$.getJSON("js/fun-tips/fun.json", function (data) {
    var $jsontip = $("#jsonTip");
    var strHtml = "";
    var num = Math.floor(Math.random()*(data.length));
    var mergePre = [];
    var FinalHtmlData = [];
    $.each(data, function (infoIndex, info) {
        strHtml = "";
        if (typeof info.letter != "undefined") {
            strHtml = "<li><font>" + info["text"] + "</font></li><li>&ldquo; " + info["letter"] + "&rdquo;&nbsp;&ndash;&nbsp;<b>" + info["name"] + "</b></li>";
        } else {
            strHtml = "<li><font>" + info["text"] + "</font></li>";
        }

        if(infoIndex < num)
        {
            mergePre.push(strHtml);
        }
        else
        {
            FinalHtmlData.push(strHtml);
        }
    })
    FinalHtmlData = FinalHtmlData.concat(mergePre);
    strHtml = FinalHtmlData.join("");
    $jsontip.html(strHtml);
})
