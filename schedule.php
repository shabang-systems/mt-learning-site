<!doctype html>
<!--(c)2018 MTLearningCenter. Created by Shabang Systems-->
<html>

<head>
	<!--metadata and data imports-->
	<meta charset="utf-8">

	<!--title of webpage(hidden to users except in tabs)-->
	<title>MT Learning Center</title>

	<!--import jQuery, popperJS, and bootstrap-->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	    crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	    crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	    crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	    crossorigin="anonymous"></script>

	<!--import fonts-->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500" rel="stylesheet">

	<!--import local style sheets-->
	<link rel="stylesheet" href="./css/global.css">

	<!--define viewport-->
	<meta name="viewport" content="width=device-widdth, initial-scale=1.0">
</head>

<body>
	<!--header row-->
	<nav class="header-group navbar navbar-expand-lg navbar-dark" style="background-color: #206471;">
		<!--header row branding-->
		<a class="navbar-brand" href="./index.html">
			<t class="text-white light-title" id="header-branding">MT Learning Center</t>
		</a>
		<!--header row mobile navigation expander button-->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
		    aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!--the rest of header navbar (container)-->
		<div class="collapse navbar-collapse" id="navbarNav">
			<!--the rest of header navbar (list)-->
			<ul class="navbar-nav">
				<!--home option-->
				<li class="nav-item active">
					<a class="nav-link text-white" id="home-nav" href="./index.html">Home</a>
				</li>
				<!--sub-services dropdown (container)-->
				<li class="nav-item dropdown">
					<!--header button-->
					<a class="nav-link dropdown-toggle text-white header-nav" href="./services.html" id="navbarDropdown" role="button" data-toggle="dropdown"
					    aria-haspopup="true" aria-expanded="false">
						Services
					</a>
					<!--dropdown area (list)-->
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item services-sub-nav" href="./services.html">
							<b>Services</b>
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item services-sub-nav" href="./services/services-1.html">Group or Private Tutorials</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item services-sub-nav" href="./services/services-2.html">Standardized Test Prep Courses</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item services-sub-nav" href="./services/services-3.html">College Application Services</a>
					</div>
				</li>
				<!--navigation item-->
				<li class="nav-item">
					<a class="nav-link text-white header-nav" href="./schedule.php">Schedule and Rates</a>
				</li>
				<!--navigation item-->
				<li class="nav-item">
					<a class="nav-link text-white header-nav" href="./registration.html">Registration</a>
				</li>
				<!--navigation item-->
				<li class="nav-item">
					<a class="nav-link text-white header-nav" href="./faq.html">FAQs</a>
				</li>
				<!--navigation item-->
				<li class="nav-item">
					<a class="nav-link text-white header-nav" href="./about.html">About</a>
				</li>
				<!--navigation item-->
				<li class="nav-item">
					<a class="nav-link text-white header-nav" href="./contact.html">Contact</a>
				</li>
			</ul>
		</div>
	</nav>
	<!--content wrapper-->
	<div class="content-wrapper">
		<!--image heading-->
		<div class="top-image-wrapper" style="position:relative">
			<img class="top-image" src="./static/header_image/schedule-rate.jpg">
			<t class="page-title" style="position: absolute; top: 35%; left: 5%; font-size:25px; color: black; text-shadow: 1px 1px white;">
				Schedules and Rates
			</t>
		</div>
		<br />
		<!--standard testimonial button-->
		<a role="button" href="./about.html#testimonials" class="btn btn-lg btn-outline-dark testimonial-button" style="white-space: normal;display:table;margin:0 auto;background-color: #206471;color:white; height:1vw">
			<t class="testimonial">If you would like to see what our students think about us, click here!</t>
		</a>
		<br />
		<!--part-1 wrapper-->
		<div class="container">
			<!--private sessions text-->
			<h2 class="light-title" style="width:95%; margin:0 auto; padding-bottom:5px">Private Sessions</h2>
			<div style="width:90%; margin:0 auto">
				<p style="margin-bottom: 10px">One-on-one Service: $70/hour, classes can be canceled or changed with one week notice.</p>
				<p style="margin-bottom: 30px">One-on-two Service; $42.5/hour/student, classes can be canceled or changed with one week notice.</p>
			</div>
		</div>
		<!--part-2 wrapper-->
		<div class="container">
			<!--group sessions text and content-->
			<h2 class="light-title" style="width:95%; margin:0 auto">Group Sessions</h2>
			<br />
			<!--school-year classes card-->
			<div class="card" style="width: 95%; margin:0 auto">
				<div class="card-body">
					<div class="container">
						<div class="row">
							<div class="col-10">
								<h5 class="card-title">Group Classes (School Year)</h5>
							</div>
							<div class="col-2">
								<div href="#" style="margin:0 auto; display:inline-block; width:100%; height:100%;" class="card-link">
									<button class="btn btn-primary dropdown-toggle" type="button" id="schoolYearSchedule" data-toggle="dropdown" aria-haspopup="true"
									    aria-expanded="false" style="position: absolute; top: 50%; left: 50%; transform: translate(-70%, -50%); background-color:#206471">Class Options</button>
									<div class="dropdown-menu" aria-labelledby="schoolYearSchedule">
                                        <?php
								            $files = scandir("static/pdf/schedules/school-year");
								            for ($i = count($files) - 1; $i >= 2; $i--) {
									            echo '<a class="dropdown-item" href="'.'static/pdf/schedules/school-year/' . $files[$i] . '">' . $files[$i] . '</a>';
								            }
								        ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br />
			<!--summer classes card-->
			<div class="card" style="width: 95%; margin:0 auto">
				<div class="card-body">
					<div class="container">
						<div class="row">
							<div class="col-10">
								<h5 class="card-title">Group Classes (Summer)</h5>
							</div>
							<div class="col-2">
								<div href="#" style="margin:0 auto; display:inline-block; width:100%; height:100%;" class="card-link">
									<button class="btn btn-primary dropdown-toggle" type="button" id="summerSchedule" data-toggle="dropdown" aria-haspopup="true"
									    aria-expanded="false" style="position: absolute; top: 50%; left: 50%; transform: translate(-70%, -50%); background-color:#206471">Class Options</button>
									<div class="dropdown-menu" aria-labelledby="summerSchedule">
                                        <?php
								            $files = scandir("static/pdf/schedules/summer");
								            for ($i = count($files) - 1; $i >= 2; $i--) {
									            echo '<a class="dropdown-item" href="'.'static/pdf/schedules/summer/' . $files[$i] . '">' . $files[$i] . '</a>';
								            }
								        ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br />
	</div>
	<!--footer group-->
	<div id="footer-group">
		<!--desktop footer (columns)-->
		<footer id="desktop-footer" style="background-color: #0F3238">
			<!--content container-->
			<div class="container" style="padding-top: 10px; padding-bottom: 10px">
				<!--content row-->
				<div class="row">
					<!--12 column grid (4 columns)-->
					<div class="col">
						<!--address container-->
						<div style="display: inline-block">
							<!--address text-->
							<h2 style="color: white; font-size: 27px; font-weight: 200">Address</h2>
							<ul class="list" style="list-style-type: none; margin: 0; padding: 0">
								<li style="color: lightgray">
									46260 Warm Springs Blvd., #505
									<br /> Fremont, CA 94539
								</li>
								<li style="color: lightgray">
									510-668-(0801/1979)
									<br /> 510-364-9201
								</li>
							</ul>
						</div>
					</div>
					<!--12 column grid (4 columns)-->
					<div class="col">
						<!--userful links container-->
						<div style="display: inline-block">
							<!--links text-->
							<h2 style="color: white; font-size: 27px; font-weight: 200">Useful Links</h2>
							<ul class="list" style="list-style-type: none; margin: 0; padding: 0">
								<li>
									<a href="https://www.collegeboard.org" class="links">CollegeBoard</a>
								</li>
								<li>
									<a href="http://www.actstudent.org/" class="links">ACT</a>
								</li>
								<li>
									<a href="http://apcentral.collegeboard.com/apc/Controller.jpf" class="links">AP Central</a>
								</li>
							</ul>
						</div>
					</div>
					<!--12 column grid (4 columns)-->
					<div class="col">
						<!--college links container-->
						<div style="display: inline-block">
							<!--college links text-->
							<h2 style="color: white; font-size: 27px; font-weight: 200">Colleges Links</h2>
							<ul class="list" style="list-style-type: none; margin: 0; padding: 0">
								<li>
									<a href="http://www.universityofcalifornia.edu/" class="links">University of California</a>
								</li>
								<li>
									<a href="http://www.calstate.edu/" class="links">California State University</a>
								</li>
								<li>
									<a href="http://www.scu.edu/" class="links">Santa Clara University</a>
								</li>
								<li>
									<a href="http://www.stanford.edu/" class="links">Stanford University</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--content container (footer's footer)-->
			<div class="container" style="padding-top: 10px; padding-bottom: 10px;">
				<!--content row-->
				<div class="row">
					<!--12 column grid (9 columns)-->
					<div class="col-9">
						<!--copyright container-->
						<div style="display: block">
							<t style="color: lightgrey; font-size: 10px">Copyright © 2018 MT Learning Center. All rights reserved.</t>
						</div>
					</div>
					<!--12 column grid (3 columns)-->
					<div class="col-3">
						<!--shabang branding-->
						<div style="display: block">
							<a href="www.shabang.cf" style="color: lightgrey; font-size: 10px; font-family: 'Roboto'">#!/ A Bangin' Shabang Project</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--mobile footer (rows)-->
		<footer id="mobile-footer" style="background-color: #0F3238">
			<!--content container-->
			<div class="container" style="padding-top: 10px; padding-bottom: 10px">
				<!--content column-->
				<div class="col">
					<!--row grid-->
					<div class="row">
						<!--address contianer-->
						<div style="display: inline-block">
							<!--address text-->
							<ul class="list" style="list-style-type: none; margin: 0; padding: 0">
								<li style="color: white; font-size: 18px; font-weight: 200">Address</li>
								<li style="color: lightgray">
									<t style="font-size: 10px">46260 Warm Springs Blvd., #505</t>
									<t style="font-size: 10px">Fremont, CA 94539</t>
								</li>
								<li style="color: lightgray">
									<t style="font-size: 10px">510-668-(0801/1979)</t>
									<t style="font-size: 10px">510-364-9201</t>
								</li>
							</ul>
						</div>
					</div>
					<br />
					<!--row grid-->
					<div class="row">
						<!--userful links container-->
						<div style="display: inline-block">
							<!--links text-->
							<ul class="list" style="list-style-type: none; margin: 0; padding: 0">
								<li style="color: white; font-size: 18px; font-weight: 200">Useful Links</li>
								<li>
									<a style="font-size: 10px" href="https://www.collegeboard.org" class="links">CollegeBoard</a>
								</li>
								<li>
									<a style="font-size: 10px" href="http://www.actstudent.org/" class="links">ACT</a>
								</li>
								<li>
									<a style="font-size: 10px" href="http://apcentral.collegeboard.com/apc/Controller.jpf" class="links">AP Central</a>
								</li>
							</ul>
						</div>
					</div>
					<br />
					<div class="row">
						<!--college links container-->
						<div style="display: inline-block">
							<!--college links text-->
							<ul class="list" style="list-style-type: none; margin: 0; padding: 0">
								<li style="color: white; font-size: 18px; font-weight: 200">Colleges Links</li>
								<li>
									<a style="font-size: 10px" href="http://www.universityofcalifornia.edu/" class="links">University of California</a>
								</li>
								<li>
									<a style="font-size: 10px" href="http://www.calstate.edu/" class="links">California State University</a>
								</li>
								<li>
									<a style="font-size: 10px" href="http://www.scu.edu/" class="links">Santa Clara University</a>
								</li>
								<li>
									<a style="font-size: 10px" href="http://www.stanford.edu/" class="links">Stanford University</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--content container (footer's footer)-->
			<div class="container" style="padding-top: 10px; padding-bottom: 10px;">
				<!--content column-->
				<div class="col">
					<!--row grid-->
					<div class="row">
						<!--copyright container-->
						<div style="display: block">
							<a href="www.shabang.cf" style="color: lightgrey; font-size: 10px; font-family: 'Roboto'">#!/ A Bangin' Shabang Project</a>
						</div>
					</div>
					<!--row grid-->
					<div class="row">
						<!--shabang branding-->
						<div style="display: block">
							<t style="color: lightgrey; font-size: 10px; font-family: 'Roboto'">#!/ A Bangin' Shabang Project </t>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
</body>

</html>